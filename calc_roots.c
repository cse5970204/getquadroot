#include <stdio.h>
#include <math.h>

//three coefficients from the user
//ax^2 + bx + c = 0
//-b + sqroot(discr) / 2a
void one_root(double a, double b) {
    //-b/2a
    double root = (b*-1)/(2*a);
    printf("The repeated root is %.2lf\n", root);
}
void two_roots(double a, double b, double discr) {
    //-b+root/2a
    //-b-root/2a
    double root1 = ((b*-1)+ sqrt(discr))/(2*a);
    double root2 = ((b*-1)- sqrt(discr))/(2*a);
    printf("The two roots are %.2lf and %.2lf.\n", root1, root2);
}

double check_discr(double a, double b, double c) {
    return b*b-4*a*c;
}

void main() {
    printf("Enter coefficients of the equation to be solved (a, b and c)\n");
    double a, b, c;
    a=0;
    while (!a) {
    printf("Enter the coefficient of x^2: ");
    scanf("%lf", &a);
    }
    printf("Enter the coefficient of x: ");
    scanf("%lf", &b);
    printf("Enter the value of the constant: ");
    scanf("%lf", &c);
    //the discriminant = b^2 - 4ac
    double discr_val = check_discr(a,b,c);
    if (discr_val <0) {
        printf("The roots are complex numbers\n");
    } else if (discr_val ==0) {
        one_root(a,b);
    } else {
        two_roots(a, b, discr_val);
    }
}
















